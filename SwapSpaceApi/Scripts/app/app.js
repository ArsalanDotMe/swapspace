﻿var app = angular.module("swapspace", ["ui.select2"]);

app.controller("HomeCtrl", [
    "$scope", "$http", function ($scope, $http) {
        $scope.disabled = false;
        $scope.submit = function () {
            $scope.disabled = true;
            $http.post("/api/swap", $scope.form).success(function (data) {
                $scope.disabled = false;
                $scope.matches = data.Data;
                if (data.Count === 0) {
                    $scope.alert = "We couldn't find any swappers for your request but we'll send you  an email as soon as a potential swapper comes up.";
                } else {
                    $scope.alert = "Congratulations! We found " + data.Count + " potential swappers for your request! We'll send you an email if more swappers come.";
                }
                $scope.form = {};
            }).error(function (reason) {
                $scope.disabled = false;
                $scope.alert = "An error occured. More informaton can be found in console log.";
                console.error(reason);
            });
        };
        $scope.courses = [];
        $scope.alert = "Loading list of courses...";
        $http.get("/api/Swap/Courses").success(function (data) {
            $scope.alert = "Courses loaded.";
            $scope.courses = data;
        }).error(function (reason) {
            $scope.alert = "An error occured while loading courses. More informaton can be found in console log.";
            console.error(reason);
        });
    }]);