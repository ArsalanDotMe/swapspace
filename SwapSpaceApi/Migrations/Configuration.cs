namespace SwapSpaceApi.Migrations
{
    using SwapSpaceApi.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SwapSpaceApi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SwapSpaceApi.Models.ApplicationDbContext context)
        {
            AddCourses(context);
        }

        private void AddCourses(ApplicationDbContext context)
        {
            Course[] courses = {
    new Course() { Code =  "CS101", Name =  "Intro. to Computer Science" },
new Course() { Code =  "MT101", Name =  "Calculus - I" },
new Course() { Code =  "SS113", Name =  "Pakistan Studies" },
new Course() { Code =  "SS111", Name =  "Islamic & Religious Studies" },
new Course() { Code =  "SS101", Name =  "English Language" },
new Course() { Code =  "EE213", Name =  "Comp. Organization & Assembly Lang." },
new Course() { Code =  "CS201", Name =  "Data Structures" },
new Course() { Code =  "CS211", Name =  "Discrete Structures" },
new Course() { Code =  "EE105", Name =  "Computer Logic Design" },
new Course() { Code =  "CS103", Name =  "Computer Programming" },
new Course() { Code =  "MT104", Name =  "Linear Algebra" },
new Course() { Code =  "SS103", Name =  "Arabic Language" },
new Course() { Code =  "SS206", Name =  "Organizational Behavior" },
new Course() { Code =  "SS207", Name =  "Philosophy" },
new Course() { Code =  "SS105", Name =  "German Language" },
new Course() { Code =  "MG211", Name =  "Total Quality Management" },
new Course() { Code =  "CS301", Name =  "Theory of Automata" },
new Course() { Code =  "CS309", Name =  "Object Oriented Analysis & Design" },
new Course() { Code =  "CS203", Name =  "Database Systems" },
new Course() { Code =  "CS205", Name =  "Operating Systems" },
new Course() { Code =  "CS409", Name =  "Data Warehousing & Data mining" },
new Course() { Code =  "CS406", Name =  "Web Programming" },
new Course() { Code =  "EE424", Name =  "Introduction to Robotics" },
new Course() { Code =  "CS495", Name =  "Fundamental of Computer Vision" },
new Course() { Code =  "MT206", Name =  "Probability & Statistics" },
new Course() { Code =  "SS217", Name =  "Mass Communication" },
new Course() { Code =  "SS211", Name =  "Comparative Religions" },
new Course() { Code =  "SS301", Name =  "International Relations" },
new Course() { Code =  "SS213", Name =  "Critical Thinking" },
new Course() { Code =  "SS127", Name =  "Sociology" },
new Course() { Code =  "CS314", Name =  "Computer Networks" },
new Course() { Code =  "EE204", Name =  "Computer Architecture" },
new Course() { Code =  "CS303", Name =  "Software Engineering" },
new Course() { Code =  "CS449", Name =  "Professional Issues in IT" },
new Course() { Code =  "CS302", Name =  "Design & Analysis of Algorithms" },
new Course() { Code =  "CS401", Name =  "Artificial Intelligence" },
new Course() { Code =  "CS440", Name =  "Software for Mobile Devices" },
new Course() { Code =  "EE314", Name =  "System Programming" },
new Course() { Code =  "CS317", Name =  "Information Retrieval" },
new Course() { Code =  "CS433", Name =  "Advanced Programming" },
new Course() { Code =  "CS501", Name =  "Advanced Analysis of Algorithms" },
new Course() { Code =  "EE502", Name =  "Advanced Computer Architecture" },
new Course() { Code =  "CS557", Name =  "Statistical Pattern Recog. & Learning" },
new Course() { Code =  "CS635", Name =  "Mobile Robotics" },
new Course() { Code =  "CS577", Name =  "Multimedia Security with Digital Watermarking" },
new Course() { Code =  "CS319", Name =  "Applied Programming" },
new Course() { Code =  "CS429", Name =  "Data Mining" },
new Course() { Code =  "SS310", Name =  "Research Methodology" },
new Course() { Code =  "CS511", Name =  "Advanced Software Engineering" },
new Course() { Code =  "CS517", Name =  "Software Project Management" },
new Course() { Code =  "CS519", Name =  "Software QA & Management" },
new Course() { Code =  "CS558", Name =  "Marketing Management" },
new Course() { Code =  "CS587", Name =  "MS Research Project I" },
new Course() { Code =  "CS588", Name =  "MS Research Project II" },
new Course() { Code =  "CS591", Name =  "MS Thesis - I" },
new Course() { Code =  "CS592", Name =  "MS Thesis - II" }
                               };

            context.Courses.AddOrUpdate(c => c.Code, courses);
        }
    }
}
