﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SwapSpaceApi.Models
{
    public class Course
    {
        [Key]
        [StringLength(5,MinimumLength=5)]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
    }
    public class RequestModel
    {
        public long Id { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [StringLength(20)]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(5, MinimumLength = 5)]
        public string CourseId { get; set; }
        [Required]
        [StringLength(1, MinimumLength = 1)]
        public string FromSection { get; set; }
        [Required]
        [StringLength(1, MinimumLength = 1)]
        public string ToSection { get; set; }
    }
}