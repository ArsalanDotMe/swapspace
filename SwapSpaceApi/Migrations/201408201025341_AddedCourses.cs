namespace SwapSpaceApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCourses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 5),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Code);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Courses");
        }
    }
}
