﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SwapSpaceApi.Models;
using SendGrid;
using System.Configuration;

namespace SwapSpaceApi.Controllers
{
    public class SwapController : ApiController
    {
        ApplicationDbContext db = ApplicationDbContext.Create();

        private void SendEmail(string email, string subject, string message)
        {
            var myMessage = new SendGridMessage();
            myMessage.AddTo(email);
            myMessage.From = new System.Net.Mail.MailAddress("matchmaker@nuswapper.com", "Matchmaker");
            myMessage.Subject = subject;
            myMessage.Text = message;
            string username = ConfigurationManager.AppSettings["sengridUsername"];
            string password = ConfigurationManager.AppSettings["sengridPassword"];
            var credentials = new NetworkCredential(username, password);
            var transport = new Web(credentials);
            transport.DeliverAsync(myMessage);
        }
        public IHttpActionResult Index(RequestModel request)
        {
            request.CourseId = request.CourseId.ToUpper();
            request.FromSection = request.FromSection.ToUpper();
            request.ToSection = request.ToSection.ToUpper();

            if (db.Requests.Where(r => r.Email == request.Email && r.CourseId == request.CourseId && r.FromSection == request.FromSection && r.ToSection == request.ToSection).Count() == 0){
                db.Requests.Add(request);
                db.SaveChanges();
            }

            var matches = from req in db.Requests where req.ToSection == request.FromSection && req.FromSection == request.ToSection && req.CourseId == request.CourseId select req;
            var emails = matches.Select(m => m.Email).Distinct();
            foreach (var email in emails)
            {
                SendEmail(email, "Potential Swapper Found!", String.Format("A potential swapper for {0} who wants to swap from section {3} to section {4} has been found. Email {1} and phone number {2}", request.CourseId, request.Email,
                    String.IsNullOrEmpty(request.PhoneNumber) ? "(not specified)" : request.PhoneNumber, request.FromSection, request.ToSection));
            }
            int count = matches.Count();
            return Ok(new
            {
                Count = count,
                Data = matches
            });
        }
        [HttpGet]
        public IEnumerable<Course> Courses()
        {
            return db.Courses.ToArray();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing == true)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
